Tạo một node follow_traj_wrap_server trong file follow_traj_wrap_server, bằng việc thay đổi file có sẵn pantilt_follow_traj.cpp. Node này cung các các dịch vụ khác nhau gồm các chức năng chính liên quan tới điều khiển robot sử dụng FollowJointTrajectory action:

Cài quỹ đạo

Xác định các thông số về dung sai

Gửi mục tiêu để di chuyển robot theo quỹ đạo

Sau đó chạy node pantilt_follow_traj_client trong file pantilt_follow_traj_client.cpp
để sử dụng các dịch vụ này.

Chạy các node bằng file launch.

Phân tích:

Cài quỹ đạo: Sử dụng quỹ đạo có sẵn trong file mẫu, tạo một Empty Request, khi nhận được request, node này sẽ bắt đầu gán các quỹ đạo điểm vào goalTraj.

Xác định thông số về dung sai: tạo một kiểu srv có các dữ liệu về dung sai: vận tốc, gia tốc của pan joint và tilt joint, dung sai về thời gian time out. Khi nhận được request, gán các thông số này vào goalTraj.

Tạo thêm một service với kiểu Empty, khi nhận được request, node sẽ tiến hành gửi goal đến FollowJointTrajectory để vận hành robot
