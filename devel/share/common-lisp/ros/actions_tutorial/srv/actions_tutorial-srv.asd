
(cl:in-package :asdf)

(defsystem "actions_tutorial-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "changescale" :depends-on ("_package_changescale"))
    (:file "_package_changescale" :depends-on ("_package"))
    (:file "changetolerance" :depends-on ("_package_changetolerance"))
    (:file "_package_changetolerance" :depends-on ("_package"))
  ))