; Auto-generated. Do not edit!


(cl:in-package actions_tutorial-srv)


;//! \htmlinclude changetolerance-request.msg.html

(cl:defclass <changetolerance-request> (roslisp-msg-protocol:ros-message)
  ((pos_tolerance
    :reader pos_tolerance
    :initarg :pos_tolerance
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (vel_tolerance
    :reader vel_tolerance
    :initarg :vel_tolerance
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0))
   (time_out_tolerance
    :reader time_out_tolerance
    :initarg :time_out_tolerance
    :type cl:float
    :initform 0.0))
)

(cl:defclass changetolerance-request (<changetolerance-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <changetolerance-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'changetolerance-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name actions_tutorial-srv:<changetolerance-request> is deprecated: use actions_tutorial-srv:changetolerance-request instead.")))

(cl:ensure-generic-function 'pos_tolerance-val :lambda-list '(m))
(cl:defmethod pos_tolerance-val ((m <changetolerance-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader actions_tutorial-srv:pos_tolerance-val is deprecated.  Use actions_tutorial-srv:pos_tolerance instead.")
  (pos_tolerance m))

(cl:ensure-generic-function 'vel_tolerance-val :lambda-list '(m))
(cl:defmethod vel_tolerance-val ((m <changetolerance-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader actions_tutorial-srv:vel_tolerance-val is deprecated.  Use actions_tutorial-srv:vel_tolerance instead.")
  (vel_tolerance m))

(cl:ensure-generic-function 'time_out_tolerance-val :lambda-list '(m))
(cl:defmethod time_out_tolerance-val ((m <changetolerance-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader actions_tutorial-srv:time_out_tolerance-val is deprecated.  Use actions_tutorial-srv:time_out_tolerance instead.")
  (time_out_tolerance m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <changetolerance-request>) ostream)
  "Serializes a message object of type '<changetolerance-request>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'pos_tolerance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'pos_tolerance))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'vel_tolerance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'vel_tolerance))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'time_out_tolerance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <changetolerance-request>) istream)
  "Deserializes a message object of type '<changetolerance-request>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'pos_tolerance) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'pos_tolerance)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'vel_tolerance) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'vel_tolerance)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time_out_tolerance) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<changetolerance-request>)))
  "Returns string type for a service object of type '<changetolerance-request>"
  "actions_tutorial/changetoleranceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'changetolerance-request)))
  "Returns string type for a service object of type 'changetolerance-request"
  "actions_tutorial/changetoleranceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<changetolerance-request>)))
  "Returns md5sum for a message object of type '<changetolerance-request>"
  "2af1b3899886c2d3e4b2a22c13581129")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'changetolerance-request)))
  "Returns md5sum for a message object of type 'changetolerance-request"
  "2af1b3899886c2d3e4b2a22c13581129")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<changetolerance-request>)))
  "Returns full string definition for message of type '<changetolerance-request>"
  (cl:format cl:nil "float64[] pos_tolerance~%float64[] vel_tolerance~%float64 time_out_tolerance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'changetolerance-request)))
  "Returns full string definition for message of type 'changetolerance-request"
  (cl:format cl:nil "float64[] pos_tolerance~%float64[] vel_tolerance~%float64 time_out_tolerance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <changetolerance-request>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'pos_tolerance) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'vel_tolerance) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <changetolerance-request>))
  "Converts a ROS message object to a list"
  (cl:list 'changetolerance-request
    (cl:cons ':pos_tolerance (pos_tolerance msg))
    (cl:cons ':vel_tolerance (vel_tolerance msg))
    (cl:cons ':time_out_tolerance (time_out_tolerance msg))
))
;//! \htmlinclude changetolerance-response.msg.html

(cl:defclass <changetolerance-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass changetolerance-response (<changetolerance-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <changetolerance-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'changetolerance-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name actions_tutorial-srv:<changetolerance-response> is deprecated: use actions_tutorial-srv:changetolerance-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <changetolerance-response>) ostream)
  "Serializes a message object of type '<changetolerance-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <changetolerance-response>) istream)
  "Deserializes a message object of type '<changetolerance-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<changetolerance-response>)))
  "Returns string type for a service object of type '<changetolerance-response>"
  "actions_tutorial/changetoleranceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'changetolerance-response)))
  "Returns string type for a service object of type 'changetolerance-response"
  "actions_tutorial/changetoleranceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<changetolerance-response>)))
  "Returns md5sum for a message object of type '<changetolerance-response>"
  "2af1b3899886c2d3e4b2a22c13581129")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'changetolerance-response)))
  "Returns md5sum for a message object of type 'changetolerance-response"
  "2af1b3899886c2d3e4b2a22c13581129")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<changetolerance-response>)))
  "Returns full string definition for message of type '<changetolerance-response>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'changetolerance-response)))
  "Returns full string definition for message of type 'changetolerance-response"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <changetolerance-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <changetolerance-response>))
  "Converts a ROS message object to a list"
  (cl:list 'changetolerance-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'changetolerance)))
  'changetolerance-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'changetolerance)))
  'changetolerance-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'changetolerance)))
  "Returns string type for a service object of type '<changetolerance>"
  "actions_tutorial/changetolerance")