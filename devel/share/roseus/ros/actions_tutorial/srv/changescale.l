;; Auto-generated. Do not edit!


(when (boundp 'actions_tutorial::changescale)
  (if (not (find-package "ACTIONS_TUTORIAL"))
    (make-package "ACTIONS_TUTORIAL"))
  (shadow 'changescale (find-package "ACTIONS_TUTORIAL")))
(unless (find-package "ACTIONS_TUTORIAL::CHANGESCALE")
  (make-package "ACTIONS_TUTORIAL::CHANGESCALE"))
(unless (find-package "ACTIONS_TUTORIAL::CHANGESCALEREQUEST")
  (make-package "ACTIONS_TUTORIAL::CHANGESCALEREQUEST"))
(unless (find-package "ACTIONS_TUTORIAL::CHANGESCALERESPONSE")
  (make-package "ACTIONS_TUTORIAL::CHANGESCALERESPONSE"))

(in-package "ROS")





(defclass actions_tutorial::changescaleRequest
  :super ros::object
  :slots (_s ))

(defmethod actions_tutorial::changescaleRequest
  (:init
   (&key
    ((:s __s) 0.0)
    )
   (send-super :init)
   (setq _s (float __s))
   self)
  (:s
   (&optional __s)
   (if __s (setq _s __s)) _s)
  (:serialization-length
   ()
   (+
    ;; float32 _s
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _s
       (sys::poke _s (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _s
     (setq _s (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass actions_tutorial::changescaleResponse
  :super ros::object
  :slots ())

(defmethod actions_tutorial::changescaleResponse
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass actions_tutorial::changescale
  :super ros::object
  :slots ())

(setf (get actions_tutorial::changescale :md5sum-) "bc08dbcb40b2ba4b05703f0913420f07")
(setf (get actions_tutorial::changescale :datatype-) "actions_tutorial/changescale")
(setf (get actions_tutorial::changescale :request) actions_tutorial::changescaleRequest)
(setf (get actions_tutorial::changescale :response) actions_tutorial::changescaleResponse)

(defmethod actions_tutorial::changescaleRequest
  (:response () (instance actions_tutorial::changescaleResponse :init)))

(setf (get actions_tutorial::changescaleRequest :md5sum-) "bc08dbcb40b2ba4b05703f0913420f07")
(setf (get actions_tutorial::changescaleRequest :datatype-) "actions_tutorial/changescaleRequest")
(setf (get actions_tutorial::changescaleRequest :definition-)
      "float32 s
---

")

(setf (get actions_tutorial::changescaleResponse :md5sum-) "bc08dbcb40b2ba4b05703f0913420f07")
(setf (get actions_tutorial::changescaleResponse :datatype-) "actions_tutorial/changescaleResponse")
(setf (get actions_tutorial::changescaleResponse :definition-)
      "float32 s
---

")



(provide :actions_tutorial/changescale "bc08dbcb40b2ba4b05703f0913420f07")


