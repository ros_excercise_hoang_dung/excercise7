;; Auto-generated. Do not edit!


(when (boundp 'actions_tutorial::changetolerance)
  (if (not (find-package "ACTIONS_TUTORIAL"))
    (make-package "ACTIONS_TUTORIAL"))
  (shadow 'changetolerance (find-package "ACTIONS_TUTORIAL")))
(unless (find-package "ACTIONS_TUTORIAL::CHANGETOLERANCE")
  (make-package "ACTIONS_TUTORIAL::CHANGETOLERANCE"))
(unless (find-package "ACTIONS_TUTORIAL::CHANGETOLERANCEREQUEST")
  (make-package "ACTIONS_TUTORIAL::CHANGETOLERANCEREQUEST"))
(unless (find-package "ACTIONS_TUTORIAL::CHANGETOLERANCERESPONSE")
  (make-package "ACTIONS_TUTORIAL::CHANGETOLERANCERESPONSE"))

(in-package "ROS")





(defclass actions_tutorial::changetoleranceRequest
  :super ros::object
  :slots (_pos_tolerance _vel_tolerance _time_out_tolerance ))

(defmethod actions_tutorial::changetoleranceRequest
  (:init
   (&key
    ((:pos_tolerance __pos_tolerance) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:vel_tolerance __vel_tolerance) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:time_out_tolerance __time_out_tolerance) 0.0)
    )
   (send-super :init)
   (setq _pos_tolerance __pos_tolerance)
   (setq _vel_tolerance __vel_tolerance)
   (setq _time_out_tolerance (float __time_out_tolerance))
   self)
  (:pos_tolerance
   (&optional __pos_tolerance)
   (if __pos_tolerance (setq _pos_tolerance __pos_tolerance)) _pos_tolerance)
  (:vel_tolerance
   (&optional __vel_tolerance)
   (if __vel_tolerance (setq _vel_tolerance __vel_tolerance)) _vel_tolerance)
  (:time_out_tolerance
   (&optional __time_out_tolerance)
   (if __time_out_tolerance (setq _time_out_tolerance __time_out_tolerance)) _time_out_tolerance)
  (:serialization-length
   ()
   (+
    ;; float64[] _pos_tolerance
    (* 8    (length _pos_tolerance)) 4
    ;; float64[] _vel_tolerance
    (* 8    (length _vel_tolerance)) 4
    ;; float64 _time_out_tolerance
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _pos_tolerance
     (write-long (length _pos_tolerance) s)
     (dotimes (i (length _pos_tolerance))
       (sys::poke (elt _pos_tolerance i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _vel_tolerance
     (write-long (length _vel_tolerance) s)
     (dotimes (i (length _vel_tolerance))
       (sys::poke (elt _vel_tolerance i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64 _time_out_tolerance
       (sys::poke _time_out_tolerance (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _pos_tolerance
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos_tolerance (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _pos_tolerance i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _vel_tolerance
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _vel_tolerance (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _vel_tolerance i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64 _time_out_tolerance
     (setq _time_out_tolerance (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass actions_tutorial::changetoleranceResponse
  :super ros::object
  :slots ())

(defmethod actions_tutorial::changetoleranceResponse
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass actions_tutorial::changetolerance
  :super ros::object
  :slots ())

(setf (get actions_tutorial::changetolerance :md5sum-) "2af1b3899886c2d3e4b2a22c13581129")
(setf (get actions_tutorial::changetolerance :datatype-) "actions_tutorial/changetolerance")
(setf (get actions_tutorial::changetolerance :request) actions_tutorial::changetoleranceRequest)
(setf (get actions_tutorial::changetolerance :response) actions_tutorial::changetoleranceResponse)

(defmethod actions_tutorial::changetoleranceRequest
  (:response () (instance actions_tutorial::changetoleranceResponse :init)))

(setf (get actions_tutorial::changetoleranceRequest :md5sum-) "2af1b3899886c2d3e4b2a22c13581129")
(setf (get actions_tutorial::changetoleranceRequest :datatype-) "actions_tutorial/changetoleranceRequest")
(setf (get actions_tutorial::changetoleranceRequest :definition-)
      "float64[] pos_tolerance
float64[] vel_tolerance
float64 time_out_tolerance
---
")

(setf (get actions_tutorial::changetoleranceResponse :md5sum-) "2af1b3899886c2d3e4b2a22c13581129")
(setf (get actions_tutorial::changetoleranceResponse :datatype-) "actions_tutorial/changetoleranceResponse")
(setf (get actions_tutorial::changetoleranceResponse :definition-)
      "float64[] pos_tolerance
float64[] vel_tolerance
float64 time_out_tolerance
---
")



(provide :actions_tutorial/changetolerance "2af1b3899886c2d3e4b2a22c13581129")


