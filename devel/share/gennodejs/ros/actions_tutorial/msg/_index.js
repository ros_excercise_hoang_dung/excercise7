
"use strict";

let pantilt_actionGoal = require('./pantilt_actionGoal.js');
let pantilt_actionAction = require('./pantilt_actionAction.js');
let pantilt_actionFeedback = require('./pantilt_actionFeedback.js');
let pantilt_actionActionResult = require('./pantilt_actionActionResult.js');
let pantilt_actionResult = require('./pantilt_actionResult.js');
let pantilt_actionActionGoal = require('./pantilt_actionActionGoal.js');
let pantilt_actionActionFeedback = require('./pantilt_actionActionFeedback.js');

module.exports = {
  pantilt_actionGoal: pantilt_actionGoal,
  pantilt_actionAction: pantilt_actionAction,
  pantilt_actionFeedback: pantilt_actionFeedback,
  pantilt_actionActionResult: pantilt_actionActionResult,
  pantilt_actionResult: pantilt_actionResult,
  pantilt_actionActionGoal: pantilt_actionActionGoal,
  pantilt_actionActionFeedback: pantilt_actionActionFeedback,
};
