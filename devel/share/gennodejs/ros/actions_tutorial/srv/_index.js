
"use strict";

let changescale = require('./changescale.js')
let changetolerance = require('./changetolerance.js')

module.exports = {
  changescale: changescale,
  changetolerance: changetolerance,
};
