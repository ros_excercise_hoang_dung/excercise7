// Auto-generated. Do not edit!

// (in-package actions_tutorial.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class changetoleranceRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.pos_tolerance = null;
      this.vel_tolerance = null;
      this.time_out_tolerance = null;
    }
    else {
      if (initObj.hasOwnProperty('pos_tolerance')) {
        this.pos_tolerance = initObj.pos_tolerance
      }
      else {
        this.pos_tolerance = [];
      }
      if (initObj.hasOwnProperty('vel_tolerance')) {
        this.vel_tolerance = initObj.vel_tolerance
      }
      else {
        this.vel_tolerance = [];
      }
      if (initObj.hasOwnProperty('time_out_tolerance')) {
        this.time_out_tolerance = initObj.time_out_tolerance
      }
      else {
        this.time_out_tolerance = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type changetoleranceRequest
    // Serialize message field [pos_tolerance]
    bufferOffset = _arraySerializer.float64(obj.pos_tolerance, buffer, bufferOffset, null);
    // Serialize message field [vel_tolerance]
    bufferOffset = _arraySerializer.float64(obj.vel_tolerance, buffer, bufferOffset, null);
    // Serialize message field [time_out_tolerance]
    bufferOffset = _serializer.float64(obj.time_out_tolerance, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type changetoleranceRequest
    let len;
    let data = new changetoleranceRequest(null);
    // Deserialize message field [pos_tolerance]
    data.pos_tolerance = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [vel_tolerance]
    data.vel_tolerance = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [time_out_tolerance]
    data.time_out_tolerance = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.pos_tolerance.length;
    length += 8 * object.vel_tolerance.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a service object
    return 'actions_tutorial/changetoleranceRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2af1b3899886c2d3e4b2a22c13581129';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] pos_tolerance
    float64[] vel_tolerance
    float64 time_out_tolerance
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new changetoleranceRequest(null);
    if (msg.pos_tolerance !== undefined) {
      resolved.pos_tolerance = msg.pos_tolerance;
    }
    else {
      resolved.pos_tolerance = []
    }

    if (msg.vel_tolerance !== undefined) {
      resolved.vel_tolerance = msg.vel_tolerance;
    }
    else {
      resolved.vel_tolerance = []
    }

    if (msg.time_out_tolerance !== undefined) {
      resolved.time_out_tolerance = msg.time_out_tolerance;
    }
    else {
      resolved.time_out_tolerance = 0.0
    }

    return resolved;
    }
};

class changetoleranceResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
    }
    else {
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type changetoleranceResponse
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type changetoleranceResponse
    let len;
    let data = new changetoleranceResponse(null);
    return data;
  }

  static getMessageSize(object) {
    return 0;
  }

  static datatype() {
    // Returns string type for a service object
    return 'actions_tutorial/changetoleranceResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd41d8cd98f00b204e9800998ecf8427e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new changetoleranceResponse(null);
    return resolved;
    }
};

module.exports = {
  Request: changetoleranceRequest,
  Response: changetoleranceResponse,
  md5sum() { return '2af1b3899886c2d3e4b2a22c13581129'; },
  datatype() { return 'actions_tutorial/changetolerance'; }
};
