# CMake generated Testfile for 
# Source directory: /home/hoangdung/VTCC_project/Excercise7_ws/src
# Build directory: /home/hoangdung/VTCC_project/Excercise7_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("mastering_ros_demo_pkg")
subdirs("mastering_ros_robot_description_pkg")
subdirs("actions_tutorial")
