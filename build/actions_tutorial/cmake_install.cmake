# Install script for directory: /home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/hoangdung/VTCC_project/Excercise7_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/srv" TYPE FILE FILES
    "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
    "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/action" TYPE FILE FILES "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/action/pantilt_action.action")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/msg" TYPE FILE FILES
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
    "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/cmake" TYPE FILE FILES "/home/hoangdung/VTCC_project/Excercise7_ws/build/actions_tutorial/catkin_generated/installspace/actions_tutorial-msg-paths.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/hoangdung/VTCC_project/Excercise7_ws/devel/include/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/roseus/ros/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/common-lisp/ros/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/gennodejs/ros/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python3" -m compileall "/home/hoangdung/VTCC_project/Excercise7_ws/devel/lib/python3/dist-packages/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python3/dist-packages" TYPE DIRECTORY FILES "/home/hoangdung/VTCC_project/Excercise7_ws/devel/lib/python3/dist-packages/actions_tutorial")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/hoangdung/VTCC_project/Excercise7_ws/build/actions_tutorial/catkin_generated/installspace/actions_tutorial.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/cmake" TYPE FILE FILES "/home/hoangdung/VTCC_project/Excercise7_ws/build/actions_tutorial/catkin_generated/installspace/actions_tutorial-msg-extras.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial/cmake" TYPE FILE FILES
    "/home/hoangdung/VTCC_project/Excercise7_ws/build/actions_tutorial/catkin_generated/installspace/actions_tutorialConfig.cmake"
    "/home/hoangdung/VTCC_project/Excercise7_ws/build/actions_tutorial/catkin_generated/installspace/actions_tutorialConfig-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/actions_tutorial" TYPE FILE FILES "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/package.xml")
endif()

