# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "actions_tutorial: 7 messages, 2 services")

set(MSG_I_FLAGS "-Iactions_tutorial:/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/noetic/share/actionlib_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(actions_tutorial_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" "actionlib_msgs/GoalID:actions_tutorial/pantilt_actionActionFeedback:actions_tutorial/pantilt_actionFeedback:actionlib_msgs/GoalStatus:actions_tutorial/pantilt_actionActionResult:actions_tutorial/pantilt_actionActionGoal:actions_tutorial/pantilt_actionGoal:std_msgs/Header:actions_tutorial/pantilt_actionResult"
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" "actions_tutorial/pantilt_actionGoal:actionlib_msgs/GoalID:std_msgs/Header"
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" "actionlib_msgs/GoalStatus:actionlib_msgs/GoalID:actions_tutorial/pantilt_actionResult:std_msgs/Header"
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" "actionlib_msgs/GoalStatus:actionlib_msgs/GoalID:actions_tutorial/pantilt_actionFeedback:std_msgs/Header"
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" ""
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" ""
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" ""
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" ""
)

get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_custom_target(_actions_tutorial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "actions_tutorial" "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)

### Generating Services
_generate_srv_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)
_generate_srv_cpp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
)

### Generating Module File
_generate_module_cpp(actions_tutorial
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(actions_tutorial_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(actions_tutorial_generate_messages actions_tutorial_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_cpp _actions_tutorial_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(actions_tutorial_gencpp)
add_dependencies(actions_tutorial_gencpp actions_tutorial_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS actions_tutorial_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_msg_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)

### Generating Services
_generate_srv_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)
_generate_srv_eus(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
)

### Generating Module File
_generate_module_eus(actions_tutorial
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(actions_tutorial_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(actions_tutorial_generate_messages actions_tutorial_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_eus _actions_tutorial_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(actions_tutorial_geneus)
add_dependencies(actions_tutorial_geneus actions_tutorial_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS actions_tutorial_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_msg_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)

### Generating Services
_generate_srv_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)
_generate_srv_lisp(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
)

### Generating Module File
_generate_module_lisp(actions_tutorial
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(actions_tutorial_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(actions_tutorial_generate_messages actions_tutorial_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_lisp _actions_tutorial_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(actions_tutorial_genlisp)
add_dependencies(actions_tutorial_genlisp actions_tutorial_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS actions_tutorial_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_msg_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)

### Generating Services
_generate_srv_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)
_generate_srv_nodejs(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
)

### Generating Module File
_generate_module_nodejs(actions_tutorial
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(actions_tutorial_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(actions_tutorial_generate_messages actions_tutorial_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_nodejs _actions_tutorial_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(actions_tutorial_gennodejs)
add_dependencies(actions_tutorial_gennodejs actions_tutorial_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS actions_tutorial_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg"
  "${MSG_I_FLAGS}"
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalStatus.msg;/opt/ros/noetic/share/actionlib_msgs/cmake/../msg/GoalID.msg;/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_msg_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)

### Generating Services
_generate_srv_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)
_generate_srv_py(actions_tutorial
  "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
)

### Generating Module File
_generate_module_py(actions_tutorial
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(actions_tutorial_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(actions_tutorial_generate_messages actions_tutorial_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionAction.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionActionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionGoal.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionResult.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/devel/share/actions_tutorial/msg/pantilt_actionFeedback.msg" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changescale.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/hoangdung/VTCC_project/Excercise7_ws/src/actions_tutorial/srv/changetolerance.srv" NAME_WE)
add_dependencies(actions_tutorial_generate_messages_py _actions_tutorial_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(actions_tutorial_genpy)
add_dependencies(actions_tutorial_genpy actions_tutorial_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS actions_tutorial_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/actions_tutorial
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(actions_tutorial_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET actionlib_msgs_generate_messages_cpp)
  add_dependencies(actions_tutorial_generate_messages_cpp actionlib_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/actions_tutorial
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(actions_tutorial_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET actionlib_msgs_generate_messages_eus)
  add_dependencies(actions_tutorial_generate_messages_eus actionlib_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/actions_tutorial
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(actions_tutorial_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET actionlib_msgs_generate_messages_lisp)
  add_dependencies(actions_tutorial_generate_messages_lisp actionlib_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/actions_tutorial
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(actions_tutorial_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET actionlib_msgs_generate_messages_nodejs)
  add_dependencies(actions_tutorial_generate_messages_nodejs actionlib_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/actions_tutorial
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(actions_tutorial_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET actionlib_msgs_generate_messages_py)
  add_dependencies(actions_tutorial_generate_messages_py actionlib_msgs_generate_messages_py)
endif()
