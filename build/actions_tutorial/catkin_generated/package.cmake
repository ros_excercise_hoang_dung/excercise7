set(_CATKIN_CURRENT_PACKAGE "actions_tutorial")
set(actions_tutorial_VERSION "0.0.0")
set(actions_tutorial_MAINTAINER "linux <jan.rosell@upc.edu>")
set(actions_tutorial_PACKAGE_FORMAT "2")
set(actions_tutorial_BUILD_DEPENDS "message_generation" "actionlib" "actionlib_msgs" "std_msgs" "roscpp" "rospy" "mastering_ros_robot_description_pkg" "turtlesim" "robot_state_publisher" "rviz" "urdf" "xacro")
set(actions_tutorial_BUILD_EXPORT_DEPENDS "message_runtime" "message_generation" "actionlib" "actionlib_msgs" "std_msgs" "roscpp" "rospy" "mastering_ros_robot_description_pkg" "turtlesim" "robot_state_publisher" "rviz" "urdf" "xacro")
set(actions_tutorial_BUILDTOOL_DEPENDS "catkin")
set(actions_tutorial_BUILDTOOL_EXPORT_DEPENDS )
set(actions_tutorial_EXEC_DEPENDS "message_runtime" "message_generation" "actionlib" "actionlib_msgs" "std_msgs" "roscpp" "rospy" "mastering_ros_robot_description_pkg" "turtlesim" "robot_state_publisher" "rviz" "urdf" "xacro")
set(actions_tutorial_RUN_DEPENDS "message_runtime" "message_generation" "actionlib" "actionlib_msgs" "std_msgs" "roscpp" "rospy" "mastering_ros_robot_description_pkg" "turtlesim" "robot_state_publisher" "rviz" "urdf" "xacro")
set(actions_tutorial_TEST_DEPENDS )
set(actions_tutorial_DOC_DEPENDS )
set(actions_tutorial_URL_WEBSITE "")
set(actions_tutorial_URL_BUGTRACKER "")
set(actions_tutorial_URL_REPOSITORY "")
set(actions_tutorial_DEPRECATED "")